(ns clodeb.matrix
  (:refer-clojure :exclude [rand vector?])
  (:import [org.adeb.matrix Matrices Matrix MatrixLike Diagonal Shape Axis]
           [org.adeb.matrix.math LinearAlgebra]))

(defn mmul
  ([^MatrixLike a] a)
  ([^MatrixLike a ^MatrixLike b]
     (Matrices/mmul a b))
  ([^MatrixLike a ^MatrixLike b & more]
     (reduce mmul (mmul a b) more)))

(defn mul
  ([^MatrixLike a] a)
  ([a b]
     (condp instance? a
       MatrixLike (Matrices/mul a (double b))
       (Matrices/mul b (double a))))
  ([a b & more]
     (reduce mul (mul a b) more)))

(defn sub
  ([a] a)
  ([a b]
     (Matrices/sub a b))
  ([a b & more]
     (reduce sub (sub a b) more)))

(defn div
  ([a] a)
  ([a b]
     (Matrices/div a b))
  ([a b & more]
     (reduce div (div a b) more)))

(defn randn
  "Normally distributed matrix"
  [rows cols]
  (Matrices/randn rows cols))

(defn rand
  ([size]
     (Matrices/rand size size))
  ([rows, cols]
     (Matrices/rand rows cols)))

(defn pow
  ([m] (Matrices/pow m 2))
  ([m p] (Matrices/pow m p)))

(defn eye
  ([size]
     (Matrices/eye size))
  ([rows cols]
     (Matrices/eye rows cols)))

(defn transpose
  ([^MatrixLike m]
     (.transpose m)))

(defn pinv
  [mat]
  (LinearAlgebra/pinv mat))

(defn mset
  ([^MatrixLike matrix row col ^double value]
     (let [copy (.copy matrix)]
       (do
         (.put copy row col value) copy)))
  ([^MatrixLike matrix ^long index ^double value]
     (let [copy (.copy matrix)]
       (do (.put copy index value) copy))))

(defn mset!
  ([^MatrixLike matrix ^long row ^long col ^double value]
     (do (.put matrix row col value) matrix))
  ([^MatrixLike matrix ^long index ^double value]
     (do (.put matrix index value) matrix)))

(defn mget
  ([^MatrixLike matrix ^long row ^long col]
     (.get matrix row col))
  ([^MatrixLike matrix ^long index]
     (.get matrix index)))

(defn first-row
  "Gets a copy of the first row of the matrix"
  ([^MatrixLike matrix]
     (if (> (.rows matrix) 0)
       (.getRow matrix 0) nil)))

(defn rest-row
  "Gets a copy of the matrix, with the first row dropped"
  ([^MatrixLike matrix]
     (if (> (.rows matrix) 0)
       (.dropRow matrix 0) nil)))

(defn shape
  ([^MatrixLike matrix]
     (let [^Shape shape (.getShape matrix)]
       [(.rows shape) (.columns shape)])))

(defn rows?
  ([^MatrixLike matrix] (not= (.rows matrix) 0)))

(defn columns?
  ([^MatrixLike matrix] (not= (.columns matrix) 0)))  

(defn scalar?
  ([^MatrixLike matrix]
     (let [[rows cols] (shape matrix)]
       (and (= rows 1) (= cols 1)))))

(defn vector?
  ([^MatrixLike matrix]
     (let [[rows cols] (shape matrix)]
       (or (= rows 1) (= cols 1)))))

(defn row-vector?
  ([^MatrixLike matrix]
     (= (.columns matrix) 1)))

(defn column-vector?
  ([^MatrixLike matrix]
     (= (.rows matrix) 1)))

(defn sum
  ([mat]
     (Matrices/sum mat))
  ([mat axis]
     (case axis
       :row (Matrices/sum mat Axis/ROW)
       :column (Matrices/sum mat Axis/COLUMN)
       nil)))

(defn mmap
  [^MatrixLike m f]
  (Matrices/apply m f))
