(ns clodeb.classifier
  (:import [org.adeb.learning Classifier Classifier Model]
           [org.adeb.matrix.distance Distance]
           [org.adeb.dataset Dataset Column]
           [org.adeb.learning.lazy KNearestNeighbors]
           [org.adeb.learning.time RandomShapeletForest]))

(defn fit
  [^Classifier classifier ^Dataset x ^Column y]
  (.fit classifier x y))

(defn predict
  ([^Model model ^Dataset dataset]
     (map #(vec [(.getValue %) (.getProbability %)]) (.predict model dataset))))

(defn knn
  ([ & {:keys [k distance] :or {k 1 distance Distance/EUCLIDEAN }}]
     (.create (KNearestNeighbors/withNeighbors 5))))
       
  
