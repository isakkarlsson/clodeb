(ns clodeb.core
    (:use [clodeb dataset frame io]))

(def synt (read-dataset "synt" :format mat :as frame))
(println (take-column synt 0 :as numeric))


(println (column-types synt))
