(ns clodeb.dataset
  (:use [clojure.java.io])
  (:import [org.adeb.dataset Dataset DenseDataset Row]
           [org.adeb.dataset.column Column DenseCategoricColumn DenseNumericColumn]
           [org.adeb.dataset.header Type]
           [org.adeb.dataset.values Value]
           [org.adeb.io CSVInputStream]))

(def dense-dataset
  "Default dataset factory"
  (DenseDataset/getFactory))

(def categoric
  "Default target factory"
  (DenseCategoricColumn/getFactory))

(def numeric
  "Default numeric column"
  (DenseNumericColumn/getFactory))
  
(defn load-csv
  ([input & {:keys [as] :or {as dense-dataset}}]
     (with-open [r (CSVInputStream. (input-stream input))]
       (.read r as))))

(defn column-type
  ([^Dataset dataset index]
     (.getType dataset index)))

(defn column-types
  ([^Dataset dataset]
     (into [] (.getTypes dataset))))

(defn drop-column
  ([^Dataset dataset index]
     (.dropColumn dataset index))
  ([^Dataset dataset index & {:keys [as]}]
     (.dropColumn dataset index as)))
     
(defn take-row
  ([^Dataset dataset row]
     (.getRow dataset row)))

(defn take-column,
  ([^Dataset dataset index]
     (.getColumn dataset index))
  ([^Dataset dataset index & {:keys [as]}]
     (.getColumn dataset index as)))

(defn value
  ([^Dataset dataset row col]
     (.get dataset row col))
  ([entity index]
     (condp instance? entity
       Column (^Column .get entity index)
       Row (^Row .get entity index)
       nil))
  ([^Value value]
     (.value value)))

(defn rows
  [dataset] (.rows dataset))

(defn columns
  [dataset] (.columns dataset))
  





  
