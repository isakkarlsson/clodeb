(ns clodeb.io
  (:use [clodeb dataset]
        [clojure.java.io])
  (:import [org.adeb.io DatasetInputStream CSVInputStream]
           [org.adeb.matrix.io DataSeriesInputStream]))

(defn csv
  [input]
  (CSVInputStream. (input-stream input)))

(defn mat
  [input]
  (DataSeriesInputStream. (input-stream input)))

(defn read-dataset
  [input & {:keys [as format]
            :or {as dense-dataset format csv}}]
  (with-open [r (format input)]
    (.read r as)))
