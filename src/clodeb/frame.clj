(ns clodeb.frame
  (:use [clojure.java.io])
  (:import [org.adeb.matrix.dataset Frame Series]))

(def frame
  (Frame/getFactory))

(def series
  (Series/getFactory))

(defn get-matrix
  ([^Frame frame]
     (.getMatrix frame)))
